import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Vipassana Meditation',
      home: VipassanaMeditation(),
    );
  }
}

class VipassanaMeditation extends StatefulWidget {
  @override
  _VipassanaMeditationState createState() => _VipassanaMeditationState();
}

class _VipassanaMeditationState extends State<VipassanaMeditation> {
  FlutterTts tts = FlutterTts();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Vipassana Meditation'),
        backgroundColor: Colors.deepOrange,
      ),
      body: Center(
        child: Column(
          children: [
            Text('Different Exercises'),
            GestureDetector(
              onTap: () {
                // Show visual aid/animation for body scan exercise.
                tts.speak('Now, we will begin the body scan exercise.');
                showBodyScanVisualAid();
              },
              child: Container(
                height: 200,
                width: 200,
                decoration: BoxDecoration(
                  color: Colors.orange,
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: AssetImage('assets/thai_fabric_1.jpeg'),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Center(
                  child: Text('Body Scan'),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                // Show visual aid/animation for breath awareness exercise.
                tts.speak('Now, we will begin the breath awareness exercise.');
                showBreathAwarenessVisualAid();
              },
              child: Container(
                height: 200,
                width: 200,
                decoration: BoxDecoration(
                  color: Colors.pink,
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: AssetImage('assets/thai_fabric_2.jpeg'),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Center(
                  child: Text('Breath Awareness'),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                // Show visual aid/animation for anapanasati exercise.
                tts.speak('Now, we will begin the anapanasati exercise.');
                showAnapanasatiVisualAid();
              },
              child: Container(
                height: 200,
                width: 200,
                decoration: BoxDecoration(
                  color: Colors.purple,
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: AssetImage('assets/thai_fabric_3.jpeg'),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Center(
                  child: Text('Anapanasati'),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                // Show visual aid/animation for metta bhavana exercise.
                tts.speak('Now, we will begin the metta bhavana exercise.');
                showMettaBhavanaVisualAid();
              },
              child: Container(
                height: 2
